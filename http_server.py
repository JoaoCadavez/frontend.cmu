from flask import Flask, request, render_template, make_response
import requests
import json

app = Flask(__name__, template_folder='templates')
app.config["DEBUG"] = True
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/', methods=['POST'])
def my_form_post():
    postal_code = request.form['postal_code']
    street_name = request.form['street_name']
    freguesia = request.form['street_id']

    group_1_content = group_1(postal_code)
    group_2_content = group_2(postal_code, street_name)
    group_3_content = group_3(freguesia, street_name)
    title = street_name + " | "+postal_code+" | "+freguesia
    return render_template('response.html', street_name=title, group_1_content=group_1_content,group_2_content=group_2_content, group_3_content=group_3_content)


def group_1(postal_code):
    url_request = 'http://g1:8000/streets?postalCode='+postal_code
    x = requests.get(url_request)
    x.encoding = 'utf-8'
    return x.text

def group_2(postal_code, street_name):
    url_request = 'http://g2:8080/streets/html?postalCode='+postal_code+'&street='+street_name
    x = requests.get(url_request)
    return x.text
        
def group_3(freguesia_name, street_name):
    url_request = 'http://g3:9595/info?freg='+freguesia_name+'&rua='+street_name
    x = requests.get(url_request)
    return x.text


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port='8181')
